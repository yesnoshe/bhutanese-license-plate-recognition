Bhutanese License Number Plate Recognition
Based on Convolutional Neural Networks, it is a system which can be used to identify and recognize Bhutanese License Number Plate.

**Aim**

Our project aims to help RSTA and public users to identify, verify and validate vehicle license number plates.

**Goal**

The goal of this system is to recognize the license number plate efficiently and accurately.

**Objectives**


To identify vehicle license plates in different lighting conditions .
To accurately identify, verify and capture the vehicle license plate.
To capture license plates in formation.


The scope of the project is within the country. Our primary aim is to initially focus on the different vehicle number plates used within the country. In this project, a model will be trained using CNN algorithms to recognize license number plates. The model with the best performance will be integrated with the website and deployed in a free hosting server. Using this system, users can
upload images of license number plate under different lighting conditions to recognize license number plate.



